/*
  FUSE: Filesystem in Userspace
  Copyright (C) 2017 Duke University

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.

*/

#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include "ct_rw.h"
#include "awsFile.h"
#include "cache.h"

static const char *userdevice_path = "/userdevice";
static const char *userdevice_path1 = "/home/esa04/ECE590_Group4_Project/build/projectmountpoint/userdevice";
static const char *targetdevice_path = "/home/esa04/sparse-file";
LRU* cache;

static int ct_access(const char *path, int mask)
{
  (void) path;
  (void) mask;
  return 0;
}

static int ct_readlink(const char *path, char *buf, size_t size)
{
  (void) path;
  (void) buf;
  (void) size;
  return 0;
}



static int ct_mknod(const char *path, mode_t mode, dev_t rdev)
{
  (void) path;
  (void) mode;
  (void) rdev;
  return 0;
}

static int ct_mkdir(const char *path, mode_t mode)
{
  (void) path;
  (void) mode;
  return 0;
}

static int ct_unlink(const char *path)
{
  (void) path;
  return 0;
}

static int ct_rmdir(const char *path)
{
  (void) path;
  return 0;
}

static int ct_symlink(const char *from, const char *to)
{
  (void) from;
  (void) to;
  return 0;
}

static int ct_rename(const char *from, const char *to)
{
  (void) from;
  (void) to;
  return 0;
}

static int ct_link(const char *from, const char *to)
{
  (void) from;
  (void) to;
  return 0;
}

static int ct_chmod(const char *path, mode_t mode)
{
  (void) path;
  (void) mode;
  return 0;
}

static int ct_chown(const char *path, uid_t uid, gid_t gid)
{
  (void) path;
  (void) uid;
  (void) gid;
  return 0;
}

static int ct_truncate(const char *path, off_t size)
{
  (void) path;
  (void) size;
  return 0;
}

static int ct_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
  (void) path;
  (void) mode;
  (void) fi;
  return 0;
}

static int ct_open(const char *path, struct fuse_file_info *fi)
{
  (void) path;
  (void) fi;
  return 0;
}

static int ct_statfs(const char *path, struct statvfs *stbuf)
{
  (void) path;
  (void) stbuf;
  return 0;
}

static int ct_release(const char *path, struct fuse_file_info *fi)
{
  (void) path;
  close(fi->fh);
  return 0;
}

static int ct_getattr(const char *path, struct stat *stbuf)
{
  int res = 0;
  struct stat st;
  stat(targetdevice_path, &st);
  off_t blocksize = BLOCK_SIZE;
  off_t cloudmaxsize = CLOUD_MAX_SIZE;
  off_t size = blocksize*cloudmaxsize;
  memset(stbuf, 0, sizeof(struct stat));
  if (strcmp(path, "/") == 0) {
    stbuf->st_mode = S_IFDIR | 0775;
    stbuf->st_nlink = 2;
    stbuf->st_gid = 1001;
    stbuf->st_uid = 990;
  } else if (strcmp(path, userdevice_path) == 0 || strcmp(path, userdevice_path1) == 0) {
    stbuf->st_mode = S_IFREG | 0664;
    stbuf->st_nlink = 1;
    stbuf->st_size = size;
    stbuf->st_blksize = BLOCK_SIZE;
    stbuf->st_blocks = CLOUD_MAX_SIZE;
    stbuf->st_gid = 1001;
    stbuf->st_uid = 990;
  } else{
    stbuf->st_mode = S_IFREG | 0664;
  }
  
  return res;
}

static int ct_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		      off_t offset, struct fuse_file_info *fi)
{
  (void) offset;
  (void) fi;
  
  if (strcmp(path, "/") != 0)
    return -ENOENT;
  
  filler(buf, ".", NULL, 0);
  filler(buf, "..", NULL, 0);
  filler(buf, userdevice_path + 1, NULL, 0);
  
  return 0;
}

static struct fuse_operations ct_oper {};

int main(int argc, char *argv[])
{
	cache = new LRU();
	cache->print_list();
	ct_oper.getattr = ct_getattr;
	ct_oper.readdir = ct_readdir;
	ct_oper.access = ct_access;
	ct_oper.readlink = ct_readlink;
	ct_oper.mknod = ct_mknod;
	ct_oper.mkdir = ct_mkdir;
	ct_oper.symlink = ct_symlink;
	ct_oper.unlink = ct_unlink;
	ct_oper.rmdir = ct_rmdir;
	ct_oper.rename = ct_rename;
	ct_oper.link = ct_link;
	ct_oper.chmod = ct_chmod;
	ct_oper.chown = ct_chown;
	ct_oper.truncate = ct_truncate;
	ct_oper.create = ct_create;
	ct_oper.read = ct_read;
	ct_oper.write = ct_write;
	ct_oper.open = ct_open;
	ct_oper.statfs = ct_statfs;
	ct_oper.release = ct_release;
	return fuse_main(argc, argv, &ct_oper, NULL);
}
