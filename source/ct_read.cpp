#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/time.h>
#include <string>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <fuse.h>
#include "ct_rw.h"
#include "awsFile.h"
#include "cache.h"
#include "cloudTierCPP.h"

using namespace std;
//MARK: detailed function to read one block
int read_one_block(size_t block_num, const char* pathname, char* buffer, size_t size, off_t offset){
  int fd;
  int res;
  char * block_path = (char *)malloc(sizeof(char)*256);
  strcpy(block_path,pathname);
  if((fd = open(pathname, O_RDONLY)) != -1){
    printf("    Block in SSD, opened\n");
    if((res = pread(fd, buffer, size, offset)) == -1){
      printf("    Block in SSD, read error\n");
      close(fd);
      return -errno;
    }
    printf("    Block in SSD, read successful\n");
    printf("    buffer = %s, pathname = %s, block_num = %lu, size = %lu, offset = %ld\n", buffer, pathname, block_num, size, offset);
    cache->update(block_num);
  }
  else if(is_block_in_cloud(block_num)==1){
    printf("\nBlock in cloud\n");
    get_cloud(block_num,block_path);
    if((fd = open(pathname, O_RDONLY)) == -1){
      printf("\nBlock in cloud, open error\n");
      return -errno;
    }
    if((res = pread(fd, buffer, size, offset)) == -1){
      printf("\nBlock in cloud, read error\n");
      close(fd);
      return -errno;
    }
    printf("\nBlock in cloud, read successful\n");
    printf("    buffer = %s, pathname = %s, block_num = %lu, size = %lu, offset = %ld\n", buffer, pathname, block_num, size, offset);
    cache->update(block_num);
  }
  else{
    printf("\nBlock not found\n");
    return 0;
  }
  close(fd);
  printf("******After modification******\n");
  cache->print_list();
  free(block_path);
  return res;
}
//MARK: interface to read into buffer with given offset, using read_one_block to read certain block
int ct_read(const char* path, char* buffer, size_t size, off_t offset, struct fuse_file_info* fi){
  //check range
  if(path == NULL){
    return fi->direct_io;
  }


  off_t end = offset + size;
  char* curr_buffer = buffer;

  //1. the first block
  size_t block_num = offset / BLOCK_SIZE;
  size_t block_head = offset % BLOCK_SIZE;
  string pathname = DIR_PATH + to_string(block_num);

  if (block_num > CLOUD_MAX_SIZE) {
    return -errno;
  }
  
  if(read_one_block(block_num, pathname.c_str(), curr_buffer, min(BLOCK_SIZE - block_head, size), block_head) < 0){
    return -errno;
  }
  offset += min(BLOCK_SIZE - block_head, size);
  curr_buffer += min(BLOCK_SIZE - block_head, size);

  //2. read all the whole blocks

  while(end - offset >= BLOCK_SIZE){
    block_num++;
    pathname = DIR_PATH + to_string(block_num);
    if(read_one_block(block_num, pathname.c_str(), curr_buffer, BLOCK_SIZE, 0) < 0){
      return -errno;
    }
    offset += BLOCK_SIZE;
    curr_buffer += BLOCK_SIZE;
  }

  //3. read the last block
  if (offset < end) {
    block_num++;
    pathname = DIR_PATH + to_string(block_num);
    if(read_one_block(block_num, pathname.c_str(), curr_buffer, end - offset, 0) < 0){
      return -errno;
    }
  }

  return size;
}
