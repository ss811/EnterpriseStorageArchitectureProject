#include <fuse.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <cerrno>
#include "ct_rw.h"
#include "awsFile.h"
#include "cache.h"
#include "cloudTierCPP.h"

using namespace std;

int write_one_block(size_t block_num, char *block_path, const char *current_buffer, size_t size, off_t offset);
int is_block_in_cloud(size_t block_num);
int is_cloud_full(size_t block_num);

int ct_write(const char *path, const char *buf, size_t size, off_t offset,
			  struct fuse_file_info *fi) {
  //  int fd;
  //  int res;

  //1. check range
  printf("\n offset of ct_write is %lu\n", offset);
  (void) path;
  (void) fi;

  //2. write the first block
  off_t end = offset + size;

  size_t block_num = offset / BLOCK_SIZE;
  size_t block_head = offset % BLOCK_SIZE;
  printf("\n block_head of ct_write is %lu\n", block_head);

  if (block_num > CLOUD_MAX_SIZE) {
    return -errno;
  }

  //  string pathname = to_string(DIR_PATH) + to_string(block_num);
  char* block_path = (char*) malloc(100 * sizeof(char));
  strcpy(block_path, DIR_PATH);
  strcat(block_path, to_string(block_num).c_str());


  const char* current_buffer = buf;
  //malloc buffer
  //  size_t ret;

  //2. write the first block
  //write_one_block algorithm that write one block whether it's on ssd or cloud or create a new block
  printf("\n now we are writing the first block\n");
  write_one_block(block_num, block_path, current_buffer, min(BLOCK_SIZE - block_head, size), block_head);

  offset += min(BLOCK_SIZE - block_head, size);
  current_buffer += min(BLOCK_SIZE - block_head, size);

  //3. write all the blocks that are fully covered in the read range
  while (offset < end && end - offset >= BLOCK_SIZE) {
    //    printf("\n now we are writing the full blocks\n");
    block_num++;
    strcpy(block_path, DIR_PATH);
    strcat(block_path, to_string(block_num).c_str());

    if ( write_one_block(block_num, block_path, current_buffer, BLOCK_SIZE, 0) != 0) {
      return -errno;
    }

    offset += BLOCK_SIZE;
    current_buffer += BLOCK_SIZE;
  }

  //4. write the last block, if any

  if (offset < end) {
    //    printf("\n now we are writing the last block\n");
    block_num++;
    strcpy(block_path, DIR_PATH);
    strcat(block_path, to_string(block_num).c_str());

    write_one_block(block_num, block_path, current_buffer, end - offset, 0);
  }
  free(block_path);
  return size;
}

int write_one_block(size_t block_num, char *block_path, const char *current_buffer, size_t size, off_t offset) {
  int fd;
  int ret;
  printf("\n within write_one_block: block_num is %lu\n", block_num);
  printf("\n within write_one_block: offset is %lu\n", offset);
  if ( ( fd = open(block_path, O_WRONLY) ) != -1) { //block is in ssd
    printf("\n within write_one_block: now writing an existing block in ssd\n");
    if ( ( ret = pwrite(fd, current_buffer, size, offset) ) == -1) {
      //write block file on ssd
      printf("reached pwrite = -1\n");
      close(fd);
      return -errno;
    }
    close(fd);
    put_cloud(block_num, block_path);
    cache->update(block_num);
  } else if (is_block_in_cloud(block_num) == 1) { //block is in cloud
    //download from cloud
    get_cloud(block_num, block_path);
    if ( (fd = open(block_path, O_WRONLY) ) == -1) {
      //open the downloaded block
      return -errno;
    }
    if ( (ret = pwrite(fd, current_buffer, size, offset) ) == -1) {
      //write block file on ssd
      close(fd);
      return -errno;
    }
    close(fd);
    put_cloud(block_num, block_path);
    cache->update(block_num);
  } else { //block is not found, new block, create a new block file on ssd and then put to cloud
    if (is_cloud_full(block_num) == 1) {
      return -errno;
    }
    if ( (fd = open(block_path, O_WRONLY | O_CREAT, 0776) ) == -1) {
      printf("within write_one_block: reached open with OCREAT error\n");
      return -errno;
    }
    printf("within write_one_block: reached open with O_CREAT, about to pwrite with size(%lu) offset (%lu)\n", size, offset);
    if ( (ret = pwrite(fd, current_buffer, size, offset) ) == -1) {
      close(fd);
      printf("reached pwrite in the last else\n");
      return -errno;
    }
    close(fd);
    put_cloud(block_num, block_path);
    cache->update(block_num);
  }
  printf("******After modification******\n");
  cache->print_list();
  return 0;
}

int is_cloud_full(size_t block_num) {
  if (block_num > CLOUD_MAX_SIZE) {
    return 1;
  }
  return 0;
}
