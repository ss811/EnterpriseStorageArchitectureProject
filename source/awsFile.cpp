#include "awsFile.h"
//Code partly from AWS S3 C++ example
//MARK:Get an object named block_num from an Amazon S3 bucket into a file with path blcok_path

int get_cloud(size_t block_num, char* block_path){
	Aws::SDKOptions options;
	Aws::InitAPI(options);

	std::stringstream ss;
	ss << block_num;
	const Aws::String key_name(ss.str().c_str());
	const Aws::String bucket_name("dukeece590testbucket2017");

	std::cout << "Downloading " << key_name << " from S3 bucket: " <<
		bucket_name << std::endl;

	const Aws::String region("us-east-2");

	Aws::Client::ClientConfiguration clientConfig;
	clientConfig.region = region;
	Aws::S3::S3Client s3_client(clientConfig);


	Aws::S3::Model::GetObjectRequest object_request;
	object_request.WithBucket(bucket_name).WithKey(key_name);

	auto get_object_outcome = s3_client.GetObject(object_request);

	if (get_object_outcome.IsSuccess())
	{
		Aws::OFStream local_file;
		local_file.open(block_path, std::ios::out | std::ios::binary);
		local_file << get_object_outcome.GetResult().GetBody().rdbuf();
		std::cout << "Done!" << std::endl;
		Aws::ShutdownAPI(options);
		return 1;
	}
	else
	{
		std::cout << "GetObject error: " <<
			get_object_outcome.GetError().GetExceptionName() << " " <<
			get_object_outcome.GetError().GetMessage() << std::endl;
		Aws::ShutdownAPI(options);
		return 0;        
	}

}
//MARK: Check if a block named as block_num is in the cloud, return 1 if true
int is_block_in_cloud(size_t block_num){
	Aws::SDKOptions options;
	Aws::InitAPI(options);

	std::stringstream ss;
	ss << block_num;
	const Aws::String key_name(ss.str().c_str());
	const Aws::String bucket_name("dukeece590testbucket2017");

	const Aws::String region("us-east-2");

	Aws::Client::ClientConfiguration clientConfig;
	clientConfig.region = region;
	Aws::S3::S3Client s3_client(clientConfig);

	Aws::S3::Model::ListObjectsRequest objects_request;
	objects_request.WithBucket(bucket_name);

	auto list_objects_outcome = s3_client.ListObjects(objects_request);

	if (list_objects_outcome.IsSuccess())
	{
		Aws::Vector<Aws::S3::Model::Object> object_list =
			list_objects_outcome.GetResult().GetContents();

		for (auto const &s3_object : object_list)
		{
			if(key_name==s3_object.GetKey()){
				return 1;
			}
		}
		return 0;
	}
	else
	{
		std::cout << "ListObjects error: " <<
			list_objects_outcome.GetError().GetExceptionName() << " " <<
			list_objects_outcome.GetError().GetMessage() << std::endl;
		return 0;
	}
	return 0;
}
//MARK: upload a file with path block_path into the cloud, named as block_num, 
int put_cloud(size_t block_num, char* block_path){
	Aws::SDKOptions options;
	Aws::InitAPI(options);

	const Aws::String region("us-east-2");

	Aws::Client::ClientConfiguration clientConfig;
	clientConfig.region = region;
	Aws::S3::S3Client s3_client(clientConfig);

	const Aws::String bucket_name = "dukeece590testbucket2017";
	std::stringstream ss;
	ss<<block_num;
	const Aws::String key_name(ss.str().c_str());
	const Aws::String file_name(block_path);

	std::cout << "Uploading " << file_name << " to S3 bucket " <<
		bucket_name << " at key " << key_name << std::endl;


	Aws::S3::Model::PutObjectRequest object_request;
	object_request.WithBucket(bucket_name).WithKey(key_name);

	// Binary files must also have the std::ios_base::bin flag or'ed in
	auto input_data = Aws::MakeShared<Aws::FStream>("PutObjectInputStream",
			file_name.c_str(), std::ios_base::in | std::ios_base::binary);

	object_request.SetBody(input_data);

	auto put_object_outcome = s3_client.PutObject(object_request);

	if (put_object_outcome.IsSuccess())
	{
		std::cout << "Done!" << std::endl;
		Aws::ShutdownAPI(options);
		return 1;
	}
	else
	{
		std::cout << "PutObject error: " <<
			put_object_outcome.GetError().GetExceptionName() << " " <<
			put_object_outcome.GetError().GetMessage() << std::endl;
		Aws::ShutdownAPI(options);
		return 0;       
	}
}
