#ifndef __CT_RW_H__
#define __CT_RW_H__
#define DIR_PATH "/home/esa04/ECE590_Group4_Project/logical_blocks/"
#define BLOCK_SIZE 4096
#define SSD_MAX_SIZE 20000
#define CLOUD_MAX_SIZE 50000 //this is the max number of blocks allowed in our system

int ct_read(const char* path, char* buffer, size_t size, off_t offset, struct fuse_file_info* fi);

int ct_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
#endif
