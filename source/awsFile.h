#ifndef AWSFILEUPDOWNLOAD
#define AWSFILEUPDOWNLOAD
#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <aws/s3/model/ListObjectsRequest.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <iostream>
#include <sstream>
#include <fstream>
int get_cloud(size_t block_num, char* block_path);
int put_cloud(size_t block_num, char* block_path);
int is_block_in_cloud(size_t block_num);
#endif
