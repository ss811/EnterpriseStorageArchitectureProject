#ifndef __CACHE_H__
#define __CACHE_H__
#include <string>
#include <cstdlib>
#include <cstdio>
#include <list>
#include <queue>
#include <vector>
#include <utility>
#include <unordered_map>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include "ct_rw.h"

using namespace std;

class CMP_TIME {
public:
    bool operator() (const pair<size_t,time_t>& end, const pair<size_t,time_t>& begin){
        return difftime(end.second, begin.second) > 0; // begin will place in front of end
    }
};

class LRU {
private:
    list<size_t> l;
    unordered_map<size_t,list<size_t>::iterator> m;

    bool check_numernic(char* name){
        for(size_t i = 0; i < strlen(name); i++){
            if(name[i] < '0' || name[i] > '9'){
                return false;
            }
        }
        return true;
    }

public:
    LRU() {
//MARK: initialize LRU data structure
        priority_queue<pair<size_t,time_t>, vector<pair<size_t,time_t>>, CMP_TIME> pq;
        DIR* dir = NULL;
        struct stat st;
        struct dirent* childDir;
        dir = opendir(DIR_PATH);
        while((childDir = readdir(dir)) != NULL){
            if(childDir->d_type == DT_REG && check_numernic(childDir->d_name)){
                string path(DIR_PATH), name(childDir->d_name);
                path += name;
                if(stat(path.c_str(), &st) != 0){
                    perror(strerror(errno));
                    continue;
                }
                pq.push(pair<size_t,time_t>({atoi(childDir->d_name), st.st_atime}));
            }
        }
        while(!pq.empty()){
            update(pq.top().first);
            pq.pop();
        }
    }
//MARK: Updata LRU data structure
    void update(size_t key) {
        if(m.count(key) != 0) {
            l.erase(m[key]);
        } 
        else if(l.size() >= SSD_MAX_SIZE) {
            string str(DIR_PATH);
            str += to_string(l.back());
            if(remove(str.c_str()) != 0){
                perror("Error deleting files\n");
            }
            m.erase(l.back());
            l.pop_back();
        }
        l.push_front(key);
        m[key] = l.begin();
    }
//MARK: Print for debug
    void print_list(){
        printf("******LRU cache content******\n");
        list<size_t>::iterator it;
        for(it = l.begin(); it != l.end(); it++){
            printf("%lu  ", *it);
        }
        printf("\n******LRU cache content ends******\n");
    }
};
#endif
