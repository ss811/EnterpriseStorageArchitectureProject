# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/esa04/ECE590_Group4_Project/source/awsFile.cpp" "/home/esa04/ECE590_Group4_Project/build/CMakeFiles/cloudTier.dir/awsFile.cpp.o"
  "/home/esa04/ECE590_Group4_Project/source/cloudTierCPP.cpp" "/home/esa04/ECE590_Group4_Project/build/CMakeFiles/cloudTier.dir/cloudTierCPP.cpp.o"
  "/home/esa04/ECE590_Group4_Project/source/ct_read.cpp" "/home/esa04/ECE590_Group4_Project/build/CMakeFiles/cloudTier.dir/ct_read.cpp.o"
  "/home/esa04/ECE590_Group4_Project/source/ct_write.cpp" "/home/esa04/ECE590_Group4_Project/build/CMakeFiles/cloudTier.dir/ct_write.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_IMPORT_EXPORT"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/esa04/ECE590_Group4_Project/source/aws-sdk-cpp/aws-cpp-sdk-s3/include"
  "/home/esa04/ECE590_Group4_Project/source/aws-sdk-cpp/aws-cpp-sdk-core/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
