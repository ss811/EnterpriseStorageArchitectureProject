#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <aws/s3/model/CreateBucketRequest.h>
#include <iostream>
#include <fstream>

/**
 * Put an object from an Amazon S3 bucket.
 */
int main()
{
    	Aws::SDKOptions options;
    	Aws::InitAPI(options);
	
        const Aws::String region("us-east-2");
	
        Aws::Client::ClientConfiguration clientConfig;
        clientConfig.region = region;
        Aws::S3::S3Client s3_client(clientConfig);

        const Aws::String bucket_name = "dukeece590testbucket2017";
        const Aws::String key_name = "testfile";
        const Aws::String file_name = "/home/vcm/project/awstestbuild/testfile.txt";

        std::cout << "Uploading " << file_name << " to S3 bucket " <<
            bucket_name << " at key " << key_name << std::endl;


        Aws::S3::Model::PutObjectRequest object_request;
        object_request.WithBucket(bucket_name).WithKey(key_name);

        // Binary files must also have the std::ios_base::bin flag or'ed in
        auto input_data = Aws::MakeShared<Aws::FStream>("PutObjectInputStream",
            file_name.c_str(), std::ios_base::in | std::ios_base::binary);

        object_request.SetBody(input_data);

        auto put_object_outcome = s3_client.PutObject(object_request);

        if (put_object_outcome.IsSuccess())
        {
            std::cout << "Done!" << std::endl;
        }
        else
        {
            std::cout << "PutObject error: " <<
                put_object_outcome.GetError().GetExceptionName() << " " <<
                put_object_outcome.GetError().GetMessage() << std::endl;
        }
    	Aws::ShutdownAPI(options);
}
